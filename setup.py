"""Package management for auto-tls"""
from setuptools import setup, find_packages


with open('README.md') as f:
    README = f.read()

with open('LICENSE') as f:
    LICENSE = f.read()

with open('requirements.txt') as f:
    REQUIREMENTS = f.readlines()

setup(
    name='auto-tls',
    version='0.1.0',
    description='A tool for easily generating certificates and keys for development',
    long_description=README,
    author='Eugene Kovalev',
    author_email='eugene@kovalev.systems',
    url='https://github.com/Abraxos/auto-tls',
    license=LICENSE,
    packages=find_packages(exclude=('tests', 'docs')),
    entry_points={'console_scripts': ['auto-tls=auto_tls.core:main']},
    install_requires=REQUIREMENTS,
)
