# Auto-TLS

A set of scripts and commands for generating certificate authorities and certs, mainly intended for quickly making certs for testing secured services.

## Setup

**Installing for Use**

```
sudo pip3 install /path/to/repo
```

### Development Setup

**Setting up the virtualenv**

Install a virtualenv and virtualenvwrapper according to the instructions here:

+ https://virtualenv.pypa.io/en/stable/
+ https://virtualenvwrapper.readthedocs.io/en/latest/

Then execute:

```
mkvirtualenv --python=$(which python3) auto-tls
```

**Installing the Package For Development**

```
(auto-tls) pip install -e /path/to/repo
```

The `-e` installs the package in place for development.

#### Testing

#### Type Validation

You can use the [`pytype`](https://github.com/google/pytype) type-hint validator. Though you will need to install the following packages in order to copile it: `apt-get install build-essential gcc g++ python3.6-dev` (or whatever python version you're using, might be `python3-dev` for example)

**Install PyType**

```
(auto-tls) pip install pytype
```

**Usage**

```
(auto-tls) pytype /path/to/repo/auto_tls/*.py
```

#### Running Tests With Basic Coverage Report

```
(auto-tls) pytest --cov auto_tls --cov-report=term-missing auto_tls/test/
```

## Usage

The general idea behind `auto-tls` is that it allows you to set up a directory in which to house a CA along with its server and client certificates/keys. The typical workflow that its designed for is that you go into a directory where you either already have a CA, or wish to start one, and then execute one of the functions such as `auto-tls gen-ca...` or `auto-tls gen-server...`.

The proper usage is generally detailed in the `--help` messages of the executable like so:

```
auto-tls gen-ca --help
```

Also, the following lists the available sub-commands

```
auto-tls --help
```

### Examples:

**Create a CA called `testing-ca`**

```
auto-tls gen-ca -n testing-ca -v 1 -u QA -o "Test Inc" -l Boston -s MA -c US
```

**Add a couple of server and a couple of client certificates to an existing CA**

Note that this needs to be executed in a directory with a CA like the one in the previous example

```
# Servers
auto-tls gen-server -n testing-server-1 -v 1 -u QA -o "Test Inc" -l Boston -s MA -c US --san IP:1.2.3.4 --san DNS:example.com
auto-tls gen-server -n testing-server-2 -v 1 -u QA -o "Test Inc" -l Boston -s MA -c US --san IP:1.2.3.5 --san DNS:example.com
# Clients
auto-tls gen-client -n testing-client-1 -v 1 -u QA -o "Test Inc" -l Boston -s MA -c US
auto-tls gen-client -n testing-client-2 -v 1 -u QA -o "Test Inc" -l Boston -s MA -c US
```

**Generate a keystore for a server**

```
auto-tls create-keystore -d servers/testing-server-1 -c ./ --export-password servers/testing-server-1/p12.pass
```

### Verification

**Verify that the certificate is properly signed by a CA**

```
openssl verify -verbose -x509_strict -CAfile ca-cert.pem client1/cert.pem
```

**Verify that the key and cert correspond**

The output of the following two commands should be identical

```
openssl x509 -noout -modulus -in client1/cert.pem| openssl md5
openssl rsa -noout -modulus -in client1/key.pem| openssl md5
```

**View the contents of a certificate**

```
openssl x509 -in client1/cert.pem -text -noout
```
