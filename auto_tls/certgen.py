from typing import List, Tuple, Dict, Union, Optional
import logging as log
from pathlib import Path

from OpenSSL import crypto
from OpenSSL.crypto import PKey, X509, X509Req, PKCS12
from typeguard import typechecked


ONE_DAY = 60 * 60 * 24
ONE_WEEK = ONE_DAY * 7
ONE_YEAR = ONE_DAY * 365
FOUR_YEARS = ONE_YEAR * 4
SAN_DNS = 'DNS'
SAN_IP = 'IP'

StrPath = Union[Path, str]


@typechecked
def create_key_pair(bits: int=4096):
    """Create a public/private key pair."""
    private_key = crypto.PKey()
    private_key.generate_key(crypto.TYPE_RSA, bits)
    return private_key


@typechecked
def _gen_name(common_name: str,
              unit: str,
              organization: str,
              locality: str,
              state: str,
              country: str) -> Dict[str, str]:
    name = {'C': country, 'CN': common_name}
    if state is not None:
        name['ST'] = state
    if locality is not None:
        name['L'] = locality
    if organization is not None:
        name['O'] = organization
    if unit is not None:
        name['OU'] = unit
    return name


@typechecked
def _write_private_key(private_key: PKey, private_key_path: Path):
    log.info("Writing private key to: %s", str(private_key_path))
    with private_key_path.open('w') as key_file:
        key_file.write(crypto.dump_privatekey(crypto.FILETYPE_PEM, private_key).decode('utf-8'))


@typechecked
def _write_cert(cert: X509, cert_path: Path):
    log.info("Writing cert to: %s", str(cert_path))
    with cert_path.open('w') as cert_file:
        cert_file.write(crypto.dump_certificate(crypto.FILETYPE_PEM, cert).decode('utf-8'))


@typechecked
def create_cert_req(private_key,
                    common_name: str,
                    unit: str=None,
                    organization: str=None,
                    locality: str=None,
                    state: str=None,
                    country: str='US') -> X509Req:
    """Create a certificate request."""
    req = X509Req()
    subj = req.get_subject()
    name = _gen_name(common_name, unit, organization, locality, state, country)
    for key, value in name.items():
        setattr(subj, key, value)

    req.set_pubkey(private_key)
    req.sign(private_key, 'sha256')
    return req


@typechecked
def create_cert(cert_req: X509Req,
                issuer_key: PKey,
                issuer_cert: Union[X509, X509Req],
                serial: int,
                not_after: int=None,
                subject_alt_names: List[Tuple[str, str]] = None) -> X509:
    """Generate a certificate given a certificate request."""
    not_before = 0
    not_after = ONE_YEAR if not_after is None else not_after
    cert = crypto.X509()
    cert.set_serial_number(serial)
    cert.gmtime_adj_notBefore(not_before)
    cert.gmtime_adj_notAfter(not_after)
    cert.set_issuer(issuer_cert.get_subject())
    cert.set_subject(cert_req.get_subject())
    cert.set_pubkey(cert_req.get_pubkey())
    cert.set_version(2)

    if subject_alt_names is not None:
        san = ", ".join(['{}:{}'.format(t, s) for t, s in subject_alt_names]).encode()
        log.debug(san)
        cert.add_extensions([
            crypto.X509Extension(b"keyUsage", False, b"Digital Signature, Non Repudiation, Key Encipherment"),
            crypto.X509Extension(b"basicConstraints", False, b"CA:FALSE"),
            crypto.X509Extension(b'extendedKeyUsage', False, b'serverAuth, clientAuth'),
            crypto.X509Extension(b'subjectAltName', False, san)
        ])

    cert.sign(issuer_key, 'sha256')
    return cert


@typechecked
def _generate_pair(issuer_key: PKey, issuer_cert: X509, sequence: int,
                   common_name: str, not_after_days: int, unit: str,
                   organization: str, locality: str, state: str, country: str,
                   subject_alt_names: List[Tuple[str, str]] = None):
    private_key = create_key_pair()
    if subject_alt_names:
        cert_req = create_cert_req(private_key, common_name=common_name, unit=unit, organization=organization,
                                   locality=locality, state=state, country=country)
    else:
        cert_req = create_cert_req(private_key, common_name=common_name,
                                   unit=unit, organization=organization, locality=locality, state=state,
                                   country=country)
    cert = create_cert(cert_req, issuer_key, issuer_cert, sequence, not_after=(not_after_days * ONE_DAY),
                       subject_alt_names=subject_alt_names)
    return private_key, cert


@typechecked
def create_key_and_cert(issuer_key: PKey,
                        issuer_cert: X509,
                        dir_path: StrPath,
                        sequence: int,
                        common_name: str,
                        not_after_days: int,
                        unit: str,
                        organization: str,
                        locality: str,
                        province: str,
                        country: str,
                        subject_alt_names: List[Tuple[str, str]] = None) -> Tuple[PKey, X509, Path, Path]:
    dir_path = Path(dir_path)
    key_path: Path = dir_path / 'key.pem'
    cert_path: Path = dir_path / 'cert.pem'
    log.info("Generating key/cert: %s", common_name)
    key, cert = _generate_pair(issuer_key,
                               issuer_cert,
                               sequence,
                               common_name,
                               not_after_days,
                               unit,
                               organization,
                               locality,
                               province,
                               country, subject_alt_names)
    # Write key/cert to files
    log.info("Writing key to: %s", str(key_path))
    _write_private_key(key, key_path)
    log.info("Writing cert to: %s", str(cert_path))
    _write_cert(cert, cert_path)
    return key, cert, key_path, cert_path


@typechecked
def _generate_ca(common_name: str,
                 not_after_days: int,
                 unit: str,
                 organization: str,
                 locality: str,
                 state: str,
                 country: str) -> Tuple[PKey, X509]:
    ca_key = create_key_pair()
    ca_req = create_cert_req(ca_key,
                             common_name=common_name,
                             unit=unit,
                             organization=organization,
                             locality=locality,
                             state=state,
                             country=country)
    ca_cert = create_cert(ca_req, ca_key, ca_req, 0, not_after=(not_after_days * ONE_DAY))
    return ca_key, ca_cert


@typechecked
def create_ca(directory: Path,
              common_name: str,
              ca_days_valid: int,
              org_unit: str,
              org: str,
              city: str,
              province: str,
              country: str) -> Tuple[PKey, X509, Path, Path]:
    key_path: Path = directory / 'ca-key.pem'
    cert_path: Path = directory / 'ca-cert.pem'
    log.info("Creating directory: %s", str(key_path.parent))
    key_path.parent.mkdir(parents=True, exist_ok=True)
    log.info("Generating certificate authority key/cert: %s", common_name)
    ca_key, ca_cert = _generate_ca(common_name=common_name,
                                   not_after_days=ca_days_valid,
                                   unit=org_unit,
                                   organization=org,
                                   locality=city,
                                   state=province,
                                   country=country)
    # Write CA to files
    log.info("Writing CA key to: {}".format(key_path))
    _write_private_key(ca_key, key_path)
    log.info("Writing CA cert to: {}".format(cert_path))
    _write_cert(ca_cert, cert_path)
    return ca_key, ca_cert, key_path, cert_path


@typechecked
def gen_keystore(key: PKey, cert: X509, ca_certs: Optional[List[X509]] = None) -> PKCS12:
    keystore: PKCS12 = PKCS12()
    keystore.set_privatekey(key)
    keystore.set_certificate(cert)
    if ca_certs is not None:
        keystore.set_ca_certificates(ca_certs)
    return keystore


@typechecked
def gen_truststore(ca_certs: List[X509] = None) -> PKCS12:
    keystore: PKCS12 = PKCS12()
    if ca_certs is not None:
        keystore.set_ca_certificates(ca_certs)
    return keystore
