from pathlib import Path
from typing import List, Union, Tuple
from os import access, R_OK, W_OK
import logging as log

from OpenSSL.crypto import PKey, X509, load_certificate, load_privatekey, FILETYPE_PEM
from typeguard import typechecked

from auto_tls.certgen import create_ca, StrPath, create_key_and_cert


@typechecked
def writeable(path_to_check: StrPath):
    return access(str(path_to_check), W_OK)


@typechecked
def readable(path_to_check: StrPath):
    return access(str(path_to_check), R_OK)


@typechecked
def assert_path_exists_and_readable(dir_path: StrPath) -> None:
    dir_path = Path(dir_path)
    assert Path(dir_path).exists(), "Required directory does not exist: {}".format(dir_path)
    assert readable(dir_path), "Required directory is not readable: {}".format(dir_path)


@typechecked
def assert_path_exists_and_writeable(dir_path: StrPath) -> None:
    dir_path = Path(dir_path)
    if not dir_path.exists():
        try:
            dir_path.mkdir(parents=True, exist_ok=True)
        except PermissionError:
            log.error("No permissions to create directory: {}".format(dir_path))
    assert Path(dir_path).exists(), "Required directory does not exist and could not be created: {}".format(dir_path)
    assert writeable(dir_path), "Required directory is not write-able: {}".format(dir_path)


@typechecked
def assert_path_exists_empty_and_writeable(dir_path: StrPath) -> None:
    assert_path_exists_and_writeable(dir_path)
    assert not [c for c in Path(dir_path).iterdir()], "Directory is not empty: {}".format(dir_path)


class Client(object):
    @typechecked
    def __init__(self: 'Client') -> None:
        self.dir_path: Path = None
        self.common_name: str = None
        self.key: PKey = None
        self.cert: X509 = None
        self.key_path: Path = None
        self.cert_path: Path = None

    @typechecked
    def with_dir(self, dir_path: Union[Path, str]) -> Union['Client', None]:
        self.dir_path = Path(dir_path)
        assert_path_exists_and_readable(self.dir_path)
        self.common_name = self.dir_path.stem
        self.key_path = self.dir_path / 'key.pem' if (self.dir_path / 'key.pem').exists()\
            else self.dir_path / 'ca-key.pem'
        self.cert_path = self.dir_path / 'cert.pem' if (self.dir_path / 'cert.pem').exists()\
            else self.dir_path / 'ca-cert.pem'
        try:
            self.key = load_privatekey(FILETYPE_PEM, self.key_path.open('rt').read())
            self.cert = load_certificate(FILETYPE_PEM, self.cert_path.open('rb').read())
        except FileNotFoundError:
            log.warn("Unable to load client from: \n\t{}\n\t".format(self.key_path, self.cert_path))
            return None
        return self

    @property
    @typechecked
    def client_report(self) -> str:
        return 'Client: {} SHA256: {}'.format(self.common_name, self.cert.digest('sha256'))

    @typechecked
    def with_info(self: 'Client',
                  issuer_key: PKey,
                  issuer_cert: X509,
                  directory: StrPath,
                  sequence: int,
                  common_name: str,
                  cert_valid_days: int,
                  org_unit: str,
                  org: str,
                  city: str,
                  province: str,
                  country: str):
        self.common_name = common_name
        self.dir_path = Path(directory) / self.common_name

        # Validation
        assert_path_exists_empty_and_writeable(self.dir_path)

        # Certificate & Key Generation
        self.key, self.cert, self.key_path, self.cert_path = create_key_and_cert(issuer_key=issuer_key,
                                                                                 issuer_cert=issuer_cert,
                                                                                 dir_path=self.dir_path,
                                                                                 sequence=sequence,
                                                                                 common_name=common_name,
                                                                                 not_after_days=cert_valid_days,
                                                                                 unit=org_unit,
                                                                                 organization=org,
                                                                                 locality=city,
                                                                                 province=province,
                                                                                 country=country)

        return self


class Server(Client):
    @typechecked
    def with_server_info(self: 'Server',
                         issuer_key: PKey,
                         issuer_cert: X509,
                         servers_dir: StrPath,
                         sequence: int,
                         common_name: str,
                         cert_valid_days: int,
                         org_unit: str,
                         org: str,
                         city: str,
                         province: str,
                         country: str,
                         subject_alt_names: List[Tuple[str, str]]) -> 'Server':
        self.common_name = common_name
        self.dir_path = Path(servers_dir) / self.common_name

        # Validation
        assert_path_exists_empty_and_writeable(self.dir_path)

        # Certificate & Key Generation
        self.key, self.cert, self.key_path, self.cert_path = create_key_and_cert(issuer_key=issuer_key,
                                                                                 issuer_cert=issuer_cert,
                                                                                 dir_path=self.dir_path,
                                                                                 sequence=sequence,
                                                                                 common_name=common_name,
                                                                                 not_after_days=cert_valid_days,
                                                                                 unit=org_unit,
                                                                                 organization=org,
                                                                                 locality=city,
                                                                                 province=province,
                                                                                 country=country,
                                                                                 subject_alt_names=subject_alt_names)

        return self

    @property
    @typechecked
    def server_report(self) -> str:
        return 'Server: {} SHA256: {}'.format(self.common_name, self.cert.digest('sha256'))


class CertificateAuthority(object):
    @typechecked
    def __init__(self: 'CertificateAuthority'):
        self.dir_path: Path = None
        self.servers_dir_path: Path = None
        self.clients_dir_path: Path = None
        self.servers: List[Server] = []
        self.clients: List[Client] = []
        self.key: PKey = None
        self.cert: X509 = None
        self.key_path: Path = None
        self.cert_path: Path = None

    @property
    @typechecked
    def sequence(self) -> int:
        try:
            return int((self.dir_path / 'sequence').open('r').read())
        except (FileNotFoundError, ValueError):
            return 0

    @property
    @typechecked
    def common_name(self: 'CertificateAuthority'):
        return self.dir_path.stem

    @property
    @typechecked
    def report(self) -> List[str]:
        r = ['Certificate Authority: {}'.format(self.common_name),
             '\t{}'.format(self.dir_path),
             '\tSHA256: {}'.format(self.cert.digest('sha256')),
             'SERVERS:']
        r.extend(['\t' + s.server_report for s in self.servers])
        r.append('CLIENTS:')
        r.extend(['\t' + c.client_report for c in self.clients])
        return r

    @typechecked
    def increment_sequence(self) -> None:
        (self.dir_path / 'sequence').open('w+').write(str(self.sequence + 1))

    @typechecked
    def with_dir(self, dir_path: StrPath) -> 'CertificateAuthority':
        self.dir_path = Path(dir_path)
        self.servers_dir_path = self.dir_path / 'servers'
        self.clients_dir_path = self.dir_path / 'clients'
        self.key_path: Path = self.dir_path / 'ca-key.pem'
        self.cert_path = self.dir_path / 'ca-cert.pem'

        assert_path_exists_and_readable(self.dir_path)
        assert_path_exists_and_readable(self.servers_dir_path)
        assert_path_exists_and_readable(self.key_path)
        assert_path_exists_and_readable(self.cert_path)

        self.key = load_privatekey(FILETYPE_PEM, self.key_path.open('rt').read())
        self.cert = load_certificate(FILETYPE_PEM, self.cert_path.open('rb').read())
        self.servers: List[Server] = [Server().with_dir(d) for d in self.servers_dir_path.iterdir() if d.is_dir() and Server().with_dir(d) is not None]
        self.clients: List[Client] = [Client().with_dir(d) for d in self.clients_dir_path.iterdir() if d.is_dir() and Client().with_dir(d) is not None]
        return self

    @typechecked
    def with_info(self,
                  dir_path: StrPath,
                  common_name: str,
                  days_valid: int,
                  org_unit: str,
                  org: str,
                  city: str,
                  province: str,
                  country: str) -> 'CertificateAuthority':
        log.info("Initializing Certificate authority with information:"
                 "\n\tDirectory: {}\n\tCommon Name: {}\n\tDays Valid: {}"
                 "\n\tOrganizational Unit: {}\n\tOrganization: {}"
                 "\n\tCity: {}\n\tProvince/State: {}\n\tCountry: {}".format(dir_path,
                                                                            common_name,
                                                                            days_valid,
                                                                            org_unit,
                                                                            org,
                                                                            city,
                                                                            province,
                                                                            country))
        self.dir_path = Path(dir_path)
        self.dir_path = self.dir_path / common_name if self.dir_path.stem != common_name else self.dir_path
        self.servers_dir_path = self.dir_path / 'servers'
        self.clients_dir_path = self.dir_path / 'clients'

        assert_path_exists_empty_and_writeable(self.dir_path)

        self.dir_path.mkdir(parents=True, exist_ok=True)
        self.servers_dir_path.mkdir(parents=True, exist_ok=True)
        self.clients_dir_path.mkdir(parents=True, exist_ok=True)
        self.key, self.cert, self.key_path, self.cert_path = create_ca(self.dir_path,
                                                                       common_name,
                                                                       days_valid,
                                                                       org_unit,
                                                                       org,
                                                                       city,
                                                                       province,
                                                                       country)
        return self

    @typechecked
    def new_server(self,
                   common_name: str,
                   cert_valid_days: int,
                   org_unit: str,
                   org: str,
                   city: str,
                   province: str,
                   country: str,
                   subject_alt_names: List[Tuple[str, str]]) -> Server:
        self.increment_sequence()
        return Server().with_server_info(issuer_key=self.key,
                                         issuer_cert=self.cert,
                                         servers_dir=self.servers_dir_path,
                                         sequence=self.sequence,
                                         common_name=common_name,
                                         cert_valid_days=cert_valid_days,
                                         org_unit=org_unit,
                                         org=org,
                                         city=city,
                                         province=province,
                                         country=country,
                                         subject_alt_names=subject_alt_names)

    @typechecked
    def add_server(self, server: Server) -> Server:
        self.servers.append(server)
        return server

    @typechecked
    def new_client(self,
                   common_name: str,
                   cert_valid_days: int,
                   org_unit: str,
                   org: str,
                   city: str,
                   province: str,
                   country: str) -> Client:
        self.increment_sequence()
        return Client().with_info(issuer_key=self.key,
                                  issuer_cert=self.cert,
                                  directory=self.clients_dir_path,
                                  sequence=self.sequence,
                                  common_name=common_name,
                                  cert_valid_days=cert_valid_days,
                                  org_unit=org_unit,
                                  org=org,
                                  city=city,
                                  province=province,
                                  country=country)

    @typechecked
    def add_client(self, client: Client) -> Client:
        self.clients.append(client)
        return client
