from tempfile import TemporaryDirectory
from pathlib import Path

from auto_tls.certificate_authority import CertificateAuthority, Server


def test_ca_creation():
    with TemporaryDirectory() as test_dir_name:
        # ca: CertificateAuthority = CertificateAuthority().with_dir(test_dir_name, 'testing-authority')
        print("Testing CA Creating in: {}".format(test_dir_name))
        ca: CertificateAuthority = CertificateAuthority().with_info(test_dir_name,
                                                                    'testing-authority',
                                                                    1, 'QA', 'Test Inc.',
                                                                    'Boston', 'MA', 'US')
        assert ca.dir_path.exists()
        assert ca.dir_path.stem == 'testing-authority'
        assert ca.servers_dir_path.exists()
        assert ca.servers_dir_path == ca.dir_path / 'servers'
        assert ca.clients_dir_path.exists()
        assert ca.clients_dir_path == ca.dir_path / 'clients'
        assert ca.key_path == ca.dir_path / 'ca-key.pem'
        assert ca.cert_path == ca.dir_path / 'ca-cert.pem'
        assert ca.cert
        assert ca.key.check()

        print("Loading CA from: {}".format(test_dir_name))
        new_ca = CertificateAuthority().with_dir(Path(test_dir_name) / 'testing-authority')
        assert new_ca.dir_path.exists()
        assert new_ca.dir_path == ca.dir_path
        assert new_ca.key_path.exists()
        assert new_ca.cert_path.exists()
        assert new_ca.cert
        assert new_ca.cert.digest('sha256') == ca.cert.digest('sha256')
        assert new_ca.key.check()


def test_server_creation():
    with TemporaryDirectory() as test_dir_name:
        ca: CertificateAuthority = CertificateAuthority().with_info(test_dir_name,
                                                                    'testing-authority',
                                                                    1, 'QA', 'Test Inc.',
                                                                    'Boston', 'MA', 'US')
        print("Testing Server Creating in: {}".format(ca.servers_dir_path))
        server = ca.add_server(ca.new_server('test-server-1', 1, 'QA', 'Test Inc.', 'Boston', 'MA', 'US',
                                             [('DNS', 'test.server.com'), ('IP', '1.2.3.4')]))
        assert server.dir_path.exists()
        assert server.dir_path.stem == 'test-server-1'
        assert server.key_path == server.dir_path / 'key.pem'
        assert server.cert_path == server.dir_path / 'cert.pem'

        server = ca.add_server(ca.new_server('test-server-2', 1, 'QA', 'Test Inc.', 'Boston', 'MA', 'US',
                                             [('DNS', 'test.server.com'), ('IP', '1.2.3.4')]))
        assert server.dir_path.exists()
        assert server.dir_path.stem == 'test-server-2'
        assert server.key_path == server.dir_path / 'key.pem'
        assert server.cert_path == server.dir_path / 'cert.pem'

        server2 = Server().with_dir(server.dir_path)
        assert server2.dir_path == server.dir_path
        assert server2.key_path == server.key_path
        assert server2.cert_path == server.cert_path
        assert server2.cert.digest('sha256') == server.cert.digest('sha256')


def test_client_creation():
    with TemporaryDirectory() as test_dir_name:
        ca: CertificateAuthority = CertificateAuthority().with_info(test_dir_name,
                                                                    'testing-authority',
                                                                    1, 'QA', 'Test Inc.',
                                                                    'Boston', 'MA', 'US')
        print("Testing Client Creating in: {}".format(ca.clients_dir_path))
        client = ca.add_client(ca.new_client('test-client-1', 1, 'QA', 'Test Inc.', 'Boston', 'MA', 'US'))
        assert client.dir_path.exists()
        assert client.dir_path.stem == 'test-client-1'
        assert client.key_path == client.dir_path / 'key.pem'
        assert client.cert_path == client.dir_path / 'cert.pem'

        client = ca.add_client(ca.new_client('test-client-2', 1, 'QA', 'Test Inc.', 'Boston', 'MA', 'US'))
        assert client.dir_path.exists()
        assert client.dir_path.stem == 'test-client-2'
        assert client.key_path == client.dir_path / 'key.pem'
        assert client.cert_path == client.dir_path / 'cert.pem'
