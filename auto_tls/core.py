import logging as log
from pathlib import Path
from typing import List, Union, Optional
from distutils.spawn import find_executable
from subprocess import run
import click

# Local module imports
from auto_tls.certificate_authority import CertificateAuthority, assert_path_exists_and_writeable, Client, writeable
from auto_tls.certgen import gen_keystore, gen_truststore

# Global logging configuration
LOG_LEVEL = {'CRITICAL': log.CRITICAL, 'ERROR': log.ERROR, 'WARNING': log.WARNING, 'INFO': log.INFO,
             'DEBUG': log.DEBUG, 'NOTSET': log.NOTSET}
log.basicConfig(format='%(levelname)s - %(module)s.%(funcName)s - [%(asctime)s]: %(message)s')


def configure_logging(log_level: str):
    log.getLogger().setLevel(LOG_LEVEL[log_level])


def which(executable_name: str) -> Union[str, None]:
    return find_executable(executable_name)


def pkcs_report(keystore_path, password):
    run(['openssl', 'pkcs12', '-info', '-passin', 'pass:' + password, '-in', keystore_path], check=True)


@click.group()
@click.option('--debug/--no-debug', default=False)
def main(debug):
    if debug:
        configure_logging('DEBUG')
        log.debug("Logging level set to debug")
    else:
        configure_logging('INFO')


@main.command('list')
@click.option('-d', '--directory', default=None, type=click.Path(exists=True, readable=True, file_okay=False,
                                                                 dir_okay=True, resolve_path=True),
              help="Path to a directory housing a CA and its constituent pays-pairs. Defaults to CWD.")
def list_ca(directory: Union[str, None]) -> None:
    """Scans for certificate authorities in either the current or a given directory and lists critical information
       about them such as signatures of client/server certs, etc."""
    directory = Path.cwd() if directory is None else Path(directory)
    print('\n'.join(CertificateAuthority().with_dir(directory).report))


@main.command('gen-ca')
@click.option('-d', '--directory', default=None, type=click.Path(exists=True, readable=True, file_okay=False,
                                                                 writable=True, dir_okay=True,
                                                                 resolve_path=True),
              help="The directory in which this CA will be created. Defaults to CWD.")
@click.option('-n', '--common-name', help="The CA's common name")
@click.option('-v', '--days-valid', type=int, help="The number of days from today that the CA will be valid")
@click.option('-u', '--org-unit', help="The organizational unit associated with the CA")
@click.option('-o', '--org', help="The organization associated with the CA")
@click.option('-l', '--city', help="The city/locality associated with the CA")
@click.option('-s', '--state', help="The state/province associated with the CA")
@click.option('-c', '--country', default='US', help="The country associated with the CA, must be TWO letters long, "
                                                    "defaults to 'US'")
def gen_ca(directory: Union[None, str],
           common_name: str,
           days_valid: int,
           org_unit: str,
           org: str,
           city: str,
           state: str,
           country: str) -> None:
    """Given an empty directory and some necessary details, this command will generate a CA certificate"""
    assert len(country) == 2, "The length of the country code must be two letter long, `{}` is invalid.".format(country)
    directory = Path.cwd() if directory is None else Path(directory)
    CertificateAuthority().with_info(directory, common_name, days_valid, org_unit, org, city, state, country)


@main.command('gen-server')
@click.option('-d', '--directory', default=None, type=click.Path(exists=True, readable=True, file_okay=False,
                                                                 writable=True, dir_okay=True,
                                                                 resolve_path=True),
              help="The directory associated with the parent CA. Defaults to CWD. Server will be created in "
                   "<ca-dir>/servers/<common-name>")
@click.option('-n', '--common-name', help="The server's common name")
@click.option('-v', '--days-valid', type=int, help="The number of days from today that the server will be valid")
@click.option('-u', '--org-unit', help="The organizational unit associated with the server")
@click.option('-o', '--org', help="The organization associated with the server")
@click.option('-l', '--city', help="The city/locality associated with the server")
@click.option('-s', '--state', help="The state/province associated with the server")
@click.option('-c', '--country', default='US', help="The country associated with the server, must be TWO letters long, "
                                                    "defaults to 'US'")
@click.option('--san', multiple=True, type=str, help="A subject-alt-name like: `--san DNS:example.com` or "
                                                     "`--san IP:1.2.3.4`")
def gen_server(directory: Union[None, str],
               common_name: str,
               days_valid: int,
               org_unit: str,
               org: str,
               city: str,
               state: str,
               country: str,
               san: List[str]) -> None:
    """Given a CA directory and some details, this command will generate a server certificate"""
    assert all([':' in n for n in san]), "All SANs must be colon-separated, like `DNS:example.com`"
    assert all([n.startswith('DNS') or n.startswith('IP') for n in san]), "All SANs must be of either type DNS or IP"
    assert len(country) == 2, "The length of the country code must be two letter long, `{}` is invalid.".format(country)
    directory = Path.cwd() if directory is None else Path(directory)
    sans = [(n.split(':', maxsplit=1)[0], n.split(':', maxsplit=1)[1]) for n in san]
    ca = CertificateAuthority().with_dir(directory)
    ca.add_server(ca.new_server(common_name, days_valid, org_unit, org, city, state, country, sans))


@main.command('gen-client')
@click.option('-d', '--directory', default=None, type=click.Path(exists=True, readable=True, file_okay=False,
                                                                 writable=True, dir_okay=True,
                                                                 resolve_path=True),
              help="The directory associated with the parent CA. Defaults to CWD. Client will be created in "
                   "<ca-dir>/clients/<common-name>")
@click.option('-n', '--common-name', help="The client's common name")
@click.option('-v', '--days-valid', type=int, help="The number of days from today that the client will be valid")
@click.option('-u', '--org-unit', help="The organizational unit associated with the client")
@click.option('-o', '--org', help="The organization associated with the client")
@click.option('-l', '--city', help="The city/locality associated with the client")
@click.option('-s', '--state', help="The state/province associated with the client")
@click.option('-c', '--country', default='US', help="The country associated with the client, must be TWO letters long, "
                                                    "defaults to 'US'")
def gen_server(directory: Union[None, str],
               common_name: str,
               days_valid: int,
               org_unit: str,
               org: str,
               city: str,
               state: str,
               country: str) -> None:
    """Given a CA directory and some details, this command will generate a server certificate"""
    assert len(country) == 2, "The length of the country code must be two letter long, `{}` is invalid.".format(country)
    directory = Path.cwd() if directory is None else Path(directory)
    ca = CertificateAuthority().with_dir(directory)
    ca.add_client(ca.new_client(common_name, days_valid, org_unit, org, city, state, country))


@main.command('create-keystore')
@click.option('-d', '--key-pair-directory', default=None, type=click.Path(writable=False, file_okay=False,
                                                                          dir_okay=True, resolve_path=True,
                                                                          exists=True, readable=True),
              help="A directory with a key-pair in it where a keystore should be created. Defaults to CWD")
@click.option('-c', '--cert-directory', default=None, type=click.Path(writable=False, file_okay=False,
                                                                      dir_okay=True, resolve_path=True,
                                                                      exists=True, readable=True),
              multiple=True, help="(Multiple) directories with certificates in them.")
@click.option('--password', prompt=True, hide_input=True,
              confirmation_prompt=True, help="The encryption password for the keystore")
@click.option('--export-password', default=None, type=click.Path(writable=True, readable=True, file_okay=True,
                                                                 dir_okay=False, resolve_path=True),
              help="Saves the password to a given filepath. WARNING: DANGEROUS, SAVES PLAINTEXT PASSWORD TO DISK")
@click.option('--report/--no-report', default=False, help='Indicates whether to let the user verify the keystore after it is created')
def create_keystore(key_pair_directory: str,
                    cert_directory: Optional[List[str]],
                    password: str,
                    export_password: Union[None, str],
                    report: bool) -> None:
    """Given a server or client directory with a key and a certificate inside, this command will create a JKS-format
       keystore."""
    key_pair_directory = Path.cwd() if key_pair_directory is None else Path(key_pair_directory)
    assert_path_exists_and_writeable(key_pair_directory)
    keystore_path = key_pair_directory / (key_pair_directory.name + '.p12')
    assert not keystore_path.exists(), "The keystore already exists, refusing to override"
    if export_password is not None:
        export_password_file = Path(export_password)
        assert not export_password_file.exists(), "The password already exists, refusing to override"
        assert writeable(export_password_file.parent)
        export_password_file.open('w+').write(password)

    clients: Optional[List[Client]] = None
    if cert_directory is not None:
        clients = [Client().with_dir(cert_dir) for cert_dir in cert_directory]
    primary = Client().with_dir(key_pair_directory)
    key_store = gen_keystore(primary.key, primary.cert, [c.cert for c in clients] if clients is not None else None)
    keystore_path.open('wb+').write(key_store.export(password))
    if report:
        pkcs_report(keystore_path, password)


@main.command('create-truststore')
@click.option('-t', '--trusted-cert-directory', default=None, type=click.Path(writable=True, file_okay=False,
                                                                              dir_okay=True, resolve_path=True,
                                                                              exists=True, readable=True),
              multiple=True,
              help="(Multiple) directories with certificates in them")
@click.option('--password', prompt=True, hide_input=True,
              confirmation_prompt=True, help="The encryption password for the keystore")
@click.option('--export-password', default=None, type=click.Path(writable=True, readable=True, file_okay=True,
                                                                 dir_okay=False, resolve_path=True),
              help="Saves the password to a given file path. WARNING: DANGEROUS, SAVES PLAINTEXT PASSWORD TO DISK")
@click.option('--report/--no-report', default=False, help='Indicates whether to let the user verify the truststore after it is created')
def create_truststore(trusted_cert_directory: List[str], password: str, export_password: Union[None, str], report: bool) -> None:
    """Given one or more certificates, this command will create a JKS-format
       truststore in the current directory."""
    truststore_path = Path().cwd() / (Path().cwd().name + '_truststore.p12')
    assert not truststore_path.exists(), "The truststore already exists, refusing to override"
    assert writeable(truststore_path.parent)
    if export_password is not None:
        export_password_file = Path(export_password)
        assert not export_password_file.exists(), "The password already exists, refusing to override"
        assert writeable(export_password_file.parent)
        export_password_file.open('w+').write(password)

    trusted = [CertificateAuthority().with_dir(cert_dir) for cert_dir in trusted_cert_directory]
    trust_store = gen_truststore([ca.cert for ca in trusted])
    truststore_path.open('wb+').write(trust_store.export(password))
    if report:
        pkcs_report(truststore_path, password)
